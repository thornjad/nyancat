package = nyancat
version = 2.0.1
tarname = $(package)
distdir = $(tarname)-$(version)

ifeq ($(shell uname -s), Darwin)
	installdir = /usr/local
else
	installdir = /usr
endif

all clean check nyancat:
	cd src && $(MAKE) $@

dist: $(distdir).tar.gz

$(distdir).tar.gz: $(distdir)
	tar chof - $(distdir) | gzip -9 -c > $@
	rm -rf $(distdir)

$(distdir): FORCE
	mkdir -p $(distdir)/src
	cp Makefile $(distdir)
	cp src/Makefile $(distdir)/src
	cp src/nyancat.c $(distdir)/src
	cp src/animation.h $(distdir)/src

FORCE:
	-rm $(distdir).tar.gz >/dev/null 2>&1
	-rm -rf $(distdir) >/dev/null 2>&1

distcheck: $(distdir).tar.gz
	gzip -cd $(distdir).tar.gz | tar xvf -
	cd $(distdir) && $(MAKE) all
	cd $(distdir) && $(MAKE) check
	cd $(distdir) && $(MAKE) clean
	rm -rf $(distdir)
	@echo "*** Package $(distdir).tar.gz is ready for distribution."

install: all
	install -d $(installdir)/bin
	install src/nyancat $(installdir)/bin/${package}
	install -d $(installdir)/share/man/man1
	gzip -9 -c < doc/nyancat.1 > $(installdir)/share/man/man1/nyancat.1.gz

.PHONY: FORCE all clean check dist distcheck install
