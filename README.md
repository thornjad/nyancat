# Nyancat CLI

Nyancat rendered in your terminal. Nothing more, nothing less. A fork of
[klange/nyancat](https://github.com/klange/nyancat).

## Setup

    make install
    nyancat

## Documentation

    man nyancat

## Thanks

This project is a fork of [klange/nyancat](https://github.com/klange/nyancat),
which is in turn a fork of
[prguitarman](http://www.prguitarman.com/index.php?id=348).

Copyright (c) 2019 Jade Michael Thornton under the terms of the [ISC
License](./LICENSE)
